//
//  SignUpViewController.swift
//  GlobalistApp
//
//  Created by Jacobo Koenig on 11/6/17.
//  Copyright © 2017 Globalist. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignUpViewController: UIViewController {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var tripsTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var countriesTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var photosTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var hotelTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
