//
//  Extensions.swift
//  Aleph Champ
//
//  Created by Jacobo Koenig on 9/4/17.
//  Copyright © 2017 Jacobo Koenig. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class Extensions {
    
    func showLoader(replace: UIView, controller: UIViewController) -> UIActivityIndicatorView {
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.color = .white
        spinner.center = replace.superview!.convert(replace.center, to: controller.view)
        spinner.startAnimating()
        
        replace.isHidden = true
        controller.view.addSubview(spinner)
        
        return spinner
    }
    
    func hideLoader(view: UIView, spinner: UIActivityIndicatorView) {
        
        view.isHidden = false
        spinner.removeFromSuperview()
        
    }
    
    func paint(view: UIImageView, color: UIColor) {
        view.image = view.image?.withRenderingMode(.alwaysTemplate)
        view.tintColor = color
    }
    
    func getUniqueItemsIn(array: [String]) -> [String] {
        var newArray: [String] = []
        for item in array {
            if !newArray.contains(item) {
                newArray.append(item)
            }
        }
        return newArray
    }
    
}

extension String {
    
    func substring(from: Int, to: Int) -> String {
        let start = self.index(self.startIndex, offsetBy: from)
        let end = self.index(self.startIndex, offsetBy: to)
        let range = start..<end
        
        return self.substring(with: range)
    }
    
    func getInitials() -> String {
        var arr = self.components(separatedBy: " ")
        if arr.count == 0 { return "" }
        return arr[0].firstLetter() + arr[arr.count - 1].firstLetter()
    }
    
    func firstLetter() -> String {
        for c in self.characters {
            return "\(c)"
        }
        
        return ""
    }
}

extension UIView {
    func makeRound() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
}

//APP SPECIFIC EXTENSIONS

class GlobalButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
    }
    
}

extension UIButton {
    
    func withDrawnBorder() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
    }
    
}

